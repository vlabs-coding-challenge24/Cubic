const btn = document.getElementById("btn");
const tbody = document.getElementById("tableBody");
const elements = tbody.getElementsByClassName("newNode");
const list = document.getElementById("drop-menu");
// Generating nav-list 
for (let i = 2; i <= 10; i++) {
    let a = document.createElement("a");
    a.setAttribute("class", "dropdown-item");
    a.innerText = i.toString();
    let li = document.createElement("li");
    li.appendChild(a);
    a.addEventListener("click", () => {
        createTable(i);
    });
    list.appendChild(li);
}
// 'Click' Button event
btn.addEventListener("click", calCube);
// use enter as click button 
document.addEventListener("keypress", (e) => {
    console.log(e);
    if (e.code == "Enter") {
        e.preventDefault();
        btn.click();
    }
});
function calCube() {
    // Check for input validation
    if (checkInput()) {
        return 0;
    }
    for (let i = 1; i <= elements.length + 1; i++) {
        let num = document.getElementById(i.toString()).innerText;
        let elres = document.getElementById("r" + i);
        let n = parseInt(num);
        let cube = n * n * n;
        elres.innerText = cube.toString();
    }
}
function tableStruct(no) {
    let row = document.createElement("tr");
    let col1 = document.createElement("th");
    let col2 = document.createElement("td");
    let col3 = document.createElement("td");
    row.setAttribute("class", "newNode");
    col1.innerText = no.toString();
    col2.setAttribute("id", no.toString());
    col2.setAttribute("contenteditable", "true");
    col3.setAttribute("id", "r" + no);
    row.appendChild(col1);
    row.appendChild(col2);
    row.appendChild(col3);
    tbody.appendChild(row);
}
function createTable(n) {
    deletePrevNode();
    for (let i = 2; i <= n; i++) {
        tableStruct(i);
    }
}
function deletePrevNode() {
    let elements = tbody.getElementsByClassName("newNode");
    while (elements.length > 0) {
        elements[0].parentNode.removeChild(elements[0]);
    }
}
function checkInput() {
    for (let i = 1; i <= elements.length + 1; i++) {
        let el = document.getElementById(i.toString()).innerText;
        //Regx to check if number
        var reg = new RegExp('[+-]?([0-9]*[.])?[0-9]+');
        let z = '0';
        if (reg.test(el) == false) {
            displayError(1);
            return 1;
        }
        else if (el == z) {
            displayError(0);
            return 1;
        }
    }
}
function displayError(e) {
    const errorCont = document.querySelector(".error-container");
    const errorMsg = errorCont.getElementsByClassName("error-msg")[0];
    if (e == 1) {
        errorMsg.textContent = "Please enter numeric values only !";
    }
    else if (e == 0) {
        errorMsg.textContent = "Please fill all elements! Don't use 0's";
    }
    errorCont.style.left = "25px";
    // errorCont.style.top= 70;
    console.log("Error please fill all elements !");
    // left 25 , top 70
    setTimeout(() => {
        errorCont.style.left = "-300px";
    }, 5000);
    return 1;
}
//# sourceMappingURL=index.js.map